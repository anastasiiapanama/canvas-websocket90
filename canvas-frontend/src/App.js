import {useEffect, useRef, useState} from "react";

const App = () => {
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: []
    });
    const [userName, setUserName] = useState('');

    const websocket = useRef(null);
    const canvas = useRef(null);

    useEffect(() => {
        websocket.current = new WebSocket('ws://localhost:8000/canvas');

        websocket.current.onmessage = event => {
            const decoded = JSON.parse(event.data);

            if (decoded.type === 'CONNECTED') {
                setUserName(decoded.userName);
            }

            if (decoded.type === 'NEW_DRAWING_POINTS') {
                setState({
                        ...state,
                        pixelsArray: decoded.state
                    });
            }
        };
    }, []);

    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            event.persist();
            const clientX = event.clientX;
            const clientY = event.clientY;
            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY
                    }]
                };
            });

            const context = canvas.current.getContext('2d');
            const imageData = context.createImageData(1, 1);
            const d = imageData.data;

            d[0] = 0;
            d[1] = 0;
            d[2] = 0;
            d[3] = 255;

            context.putImageData(imageData, event.clientX, event.clientY);
        }
    };

    const mouseDownHandler = event => {
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = event => {
        websocket.current.send(JSON.stringify({type: 'DRAWING_POINTS', state: state.pixelsArray}));
        setState({...state, mouseDown: false, pixelsArray: []});
    };

    const changeUsername = () => {
        websocket.current.send(JSON.stringify({type: 'CHANGE_USERNAME', userName}));
    };

    return (
        <div className="App">
            <input type="text" value={userName} onChange={e => setUserName(e.target.value)}/>
            <button onClick={changeUsername}>Change username</button>
            <br/>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};

export default App;
