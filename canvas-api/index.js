const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};

app.ws('/canvas', (ws, req) => {
    const id = nanoid();
    console.log('Client connected!, id=', id);
    activeConnections[id] = ws;

    let userName = 'Anonymous';
    ws.send(JSON.stringify({type: 'CONNECTED', userName}));

    ws.on('message', canvas => {
        console.log('Draw starting', canvas);
        const decoded = JSON.parse(canvas);

        if (decoded.type === 'CHANGE_USERNAME') {
            userName = decoded.userName;
        }

        if (decoded.type === 'DRAWING_POINTS') {
            Object.keys(activeConnections).forEach(key => {
                const connection = activeConnections[key];

                connection.send(JSON.stringify({
                    type: 'NEW_DRAWING_POINTS',
                    state: decoded.state
                }))
            });
        }
    });

    ws.on('close', () => {
        console.log('Client disconnected!');
    });
});

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});


